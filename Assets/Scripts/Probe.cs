﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Probe : MonoBehaviour {

	public Mass mass;
	public RectTransform connectedPanel;
	public Camera mainCamera;
	Text iron;
	Text plasma;
	public bool selected;
	bool somewhere;

	void Start()
	{
		mainCamera = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<Camera>();
		GameObject panel = GameObject.FindGameObjectWithTag ("ProbePanel");
		connectedPanel = panel.GetComponent<RectTransform>();
		iron = connectedPanel.GetChild(0).GetComponent<Text>();
		plasma = connectedPanel.GetChild(1).GetComponent<Text>();
	}

	void Update()
	{
		GameObject[] allProbes = GameObject.FindGameObjectsWithTag ("Probe");
		somewhere = false;
		for(int i = 0; i < allProbes.Length; i++)
		{
			if (allProbes[i].GetComponent<Probe>().selected) 
			{
				somewhere = true;
				Debug.Log ("Found");
			}
		}
		if (selected == true && somewhere == true) {
			connectedPanel.position = mainCamera.WorldToScreenPoint (new Vector3 (transform.position.x + 3, transform.position.y, 0));
			Planet planet = mass.gravity.gameObject.transform.parent.GetComponent<Planet> ();
			iron.text = "Iron: " + planet.iron;
			plasma.text = "Plasma: " + planet.plasma;
		} 
		if(somewhere == false)
		{
			connectedPanel.position = new Vector2(-200, -200);
		}
	}

	void OnMouseOver()
	{
		selected = true;
	}

	void OnMouseExit()
	{
		selected = false;
	}

}
