﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public int iron;
	public int plasma;
	public Text ironTxt;

	void Update()
	{
		ironTxt.text = "Iron: " + iron; 	
	}

}
