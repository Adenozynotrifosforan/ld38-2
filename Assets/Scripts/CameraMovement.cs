﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

	public float sensitivity;

	void Update () {
		if(Input.GetMouseButton(1)){
			transform.Translate (Vector3.up * Input.GetAxis("Mouse Y") * sensitivity);
			transform.Translate (Vector3.right * Input.GetAxis("Mouse X") * sensitivity);
		}
	}
}
