﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mass : MonoBehaviour {

	public Transform gravity;
	public float speed;
	public float acceleration;
	Vector3 fwd;
	public float rayLenght;
	public bool launch;

	void Update () {
		fwd = transform.forward;

		if(gravity != null ){
			if(launch == false){
			transform.LookAt(gravity.position);
			}
			if (!Physics.Raycast (transform.position, fwd, rayLenght)) {
				speed += acceleration;
			}
			else{
				speed = 0f;
			}
		}

		transform.Translate (Vector3.forward * speed * Time.deltaTime);

	}

	void OnTriggerEnter(Collider other)
	{
		
		if(speed <= 0 && launch == false)
		{
			speed = -speed;
		}

		//if(launch == false){
		gravity = other.transform;
		//}
	}

	void OnTriggerExit()
	{
		gravity = null;
		launch = false;
	}
}
