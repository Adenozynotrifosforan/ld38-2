﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Miner : MonoBehaviour {

	public Mass mass;
	public Camera mainCamera;
	public GameManager GM;

	void Start()
	{
		mass = gameObject.GetComponent<Mass>();
		mainCamera = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<Camera>();
		GM = GameObject.FindGameObjectWithTag ("GameManager").GetComponent<GameManager>();
	}

	void Update()
	{
		Planet planet = mass.gravity.gameObject.transform.parent.GetComponent<Planet> ();
		if(planet != null && planet.iron > 0)
		{
			GM.iron++;
			planet.iron--;
		}
	}

}
