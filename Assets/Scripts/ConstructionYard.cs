﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConstructionYard : MonoBehaviour {

	public RectTransform connectedPanel;
	public Camera mainCamera;
	public GameObject sonda;
	bool sondaBool;
	public Quaternion rot;
	public Transform arrow;
	public GameObject probe;
	public GameObject digger;
	public GameManager GM;

	void Start()
	{
		GM = GameObject.FindGameObjectWithTag ("GameManager").GetComponent<GameManager>();
		rot = transform.rotation;
	}

	// Update is called once per frame
	void Update () {
		connectedPanel.position =  mainCamera.WorldToScreenPoint(new Vector3(transform.position.x + 3, transform.position.y, 0));
		if(sondaBool == true)
		{
			arrow.rotation = rot;
			if(Input.GetKey(KeyCode.D))
			{
				rot.eulerAngles = new Vector3(rot.eulerAngles.x + 1, rot.eulerAngles.y , rot.eulerAngles.z);
			}
			if(Input.GetKey(KeyCode.A))
			{
				rot.eulerAngles = new Vector3(rot.eulerAngles.x - 1, rot.eulerAngles.y , rot.eulerAngles.z);
			}

			if(Input.GetKeyDown(KeyCode.E)){
				Instantiate <GameObject> (sonda, arrow.position - arrow.forward * 2,  rot);
				Debug.Log (mainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x , Input.mousePosition.y, 10)));
				sondaBool = false;
				rot = transform.rotation;
			}
		}
	}

	public void Sonda()
	{
		rot = transform.rotation;
		sondaBool = true;
		//Instantiate (sonda, transform.position - transform.forward * 2,  transform.rotation);
		Debug.Log ("Sonda");
		sonda = probe;
	}
	public void Kopacz()
	{
		rot = transform.rotation;
		sondaBool = true;
		//Instantiate (sonda, transform.position - transform.forward * 2,  transform.rotation);
		Debug.Log ("Kopacz");
		sonda = digger;
	}
		
		
}
